# dialema

A script that facilitates interaction with llama.cpp

This is a bash script that provides an easy-to-use interface for interacting with llama.cpp and the belonging ggml models. The script is still wip, and it's primarily intended for practicing bash scripting, so there's no guarantee of long-term support.


[![the main menu](https://i.postimg.cc/6pnf7Hj0/Screenshot-2023-05-22-05-15-04-3840x2160.png)](https://postimg.cc/MnZQNY8M)

## Features

- Automatic search for model files in various locations
- Easy customization options through a 'dialog' TUI (Text-based User Interface)
- Support for OpenAI API integration (imitating Chat UI)
- Save and load configuration profiles
- Help menu displaying available flags from corresponding `./main --help`

**Note:** More features are planned to be added soon, such as adjusting the search string or filter directly from the menu, automatic detection of ggml file versions (V1, V2 or V3), running chat interfaces directly as a TUI within the same menu, using ncurses library etc.

## Usage

To use this script:

1. Clone this repository.
2. Make sure you have all necessary dependencies installed - 
    (mainly dialog and jq, but is probably already installed on most linux systems).
3. Move `dialema.sh` into the same folder as llama.cpp
4. Run `chmod +x dialema.sh` to make it executable.
5. Execute `./dialema.sh`.

The main Text-based User Interface will appear where you can choose your preferred options by following on-screen instructions.

### Customization Options

Users are encouraged to customize this script according to their needs since it's designed with readability in mind.

More customization options will come soon; meanwhile feel free to modify any part of the code that suits your requirements better!

### Planned Features & Improvements

Some upcoming features include:

* Automatically detecting which version ggml files belong to (V1, V2 or V3).
* Running chat interfaces directly as a TUI within the same menu.
* Management of prompt files: Store, load and Injecting them into chat sessions. But this could be also the initial idea for developing another new small TUI app using ncurses library. Such an app will integrate this script or his functions. But I'm not sure.

## Contributing

Contributions are welcome! If you have any suggestions, improvements or bug reports, please feel free to open an issue or submit a pull request on GitHub.