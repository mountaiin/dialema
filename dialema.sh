#!/bin/bash



mkdir -p camalls_files
mkdir -p camalls_files/sessions
mkdir -p camalls_files/profiles
touch camalls_files/sessions/chatlog.txt
#mkdir -p camalls_files/profiles/tmp

mktemp



# --------------------------



############################
#   define all VARIABLES   #
############################



DECIDE="0"
MODEL=""
TEMP="0.8"
TOP_P="0.4"
MAX_TOKENS="153"
PRESENCE_PENALTY="0"
FREQUENCY_PENALTY="0"
SAVE_SESSION="0"
COLOR="1"
MMLOCK="0"
NO_MMAP="0"
MIROSTAT_1="0"
#OPENAI_API_KEY=""
ADVANCED_FLAGS=""
FILE_PATH=""


call_openai_request_tui=0
checked_options="" # we will need this later to save the options 
tempfile="chatlog.txt" # this will contain the current chat



# ---------------------------------



###################################
#   now we define our FUNCTIONS   #
###################################



# find .bin files that are bigger than 1000M in a current directory \
#
function find_files() {
    find $1 -type f -iname "*ggml*.bin" -size +1000M 2>/dev/null # please feel free \
    # to adapt file size to your needs. for instance, with 1000M \
    # very small modells will not be found
}





# calculation of center text TODO Chat-TUI
#
function center_text() {
  local screen_width=80
  local text="$1"
  local text_length=${#text}
  local padding=$(( (screen_width - text_length) / 2 ))
  local centered_text=""

  for (( i=0; i<$padding; i++ )); do
    centered_text+=" "
  done

  centered_text+="$text"
  echo "$centered_text"
}





# add vertical padding TODO Chat-TUI
#
function add_vertical_padding() {
  local total_lines=$1
  local padding_lines=$(( (total_lines - 2) / 2 )) # 2 is the number of lines in text content
  local padding=""

  for (( i=0; i<$padding_lines; i++ )); do
    padding+="\n"
  done

  echo "$padding"
}





# show message
#
function show_searching_message() {
  local line1=$(center_text "Suche nach Modellen läuft. Bitte warten...")
  #local line2=$(center_text "(could take a while...)")
  local vertical_padding=$(add_vertical_padding 28) # 28 is the height of the dialog

  dialog --infobox "$vertical_padding$line1\n$line2$vertical_padding" 28 80
  sleep 2
}





# search for models on various locations
#
function search_for_models() {
    locations=("$@")
    local all_files=""
    for loc in "${locations[@]}"; do
        local new_files=$(find_files "$loc")
        if [ -n "$new_files" ]; then
            if [ -z "$all_files" ]; then
                all_files="$new_files"
            else
                all_files="$all_files"$'\n'"$new_files"
            fi
        fi
    done
    echo "$all_files"
}





# choose a model from the list/menu
#
function choose_model() {
    show_searching_message &
    search_pid=$!
    files=$(search_for_models ~ "/media" "/mnt" "/Volumes" "/run/media" "/media/*/")
    kill $search_pid > /dev/null 2>&1
    wait $search_pid > /dev/null 2>&1
    if [ -z "$files" ]; then
        echo "Die automatische Suche konnte keine passenden Modelle finden."
        read -p "Bitte gib den Pfad zu deinem Modell an: " MODEL
        DECIDE="1"
    else
        # dialog menu with OpenAI option plus the previously found local models
        #
        files_array=($files)
        short_names=()
        for file in "${files_array[@]}"; do
            short_name=$(basename "$file")
            short_names+=("$short_name")
        done
        options=("gpt-3.5-turbo" "${short_names[@]}")
        menu_items=""
        for i in "${!options[@]}"; do
            menu_items="${menu_items} $((i+1)) \"${options[$i]}\""
        done
        cmd="dialog --stdout --no-cancel --menu \"Wähle ein Modell aus der Liste aus:\" 28 80 15 ${menu_items}"
        selected_index=$(eval $cmd)

        if [ -z "$selected_index" ] || [ "$selected_index" -le 0 ] || [ "$selected_index" -gt "${#options[@]}" ]; then
            echo "Ungültige Auswahl. Beende das Skript."
            exit 1
        fi

        if [ "$selected_index" == "1" ]; then
            DECIDE="0"
        else
            DECIDE="1"
            selected_model="${files_array[$((selected_index-2))]}" # zugriff auf richtiges element im files_array

            if [ "$selected_model" != "gpt-3.5-turbo" ]; then # create a symlink only if the model is not gpt-3.5-turbo TODO add GPT-4
                ln -sf "$selected_model" ./models
            fi

            MODEL=$(basename "$selected_model" .bin)
        fi
    fi
}





# this function is needed to read the chat_history.txt TODO
#
function read_chat_history() {
  tempfile="$1"
  chat_history=""
  while IFS= read -r line; do
    role=$(echo "$line" | awk -F ': ' '{print $1}')
    content=$(echo "$line" | awk -F ': ' '{print $2}')
    chat_history="${chat_history}{\"role\": \"${role}\", \"content\": \"${content}\"}, "
  done < "$tempfile"
  echo "[${chat_history%??}]"
}





# reset the tempfile TODO for chat tui 
#
function reset_tempfile() {
  tempfile="$1"
  echo -n "" > "$tempfile"
}





# create a dialog menu with basic options
# 
function basic_flags_menu() {
  checked_options=$(dialog --title "Basic flags" --no-cancel --checklist "Enable or disable options" 28 80 8 \
  "color" "Different colors for user and LLM" ON \
  "mlock" "Memory lock" OFF \
  "no-mmap" "Disable memory mapping" OFF \
  "mirostat-1" "Mirostat adjust output dynamically" OFF \
  3>&1 1>&2 2>&3)
  COLOR=""
  MMLOCK=""
  NO_MMAP=""
  MIROSTAT_1=""
  for option in $checked_options; do
    case $option in
      "color")        COLOR="--color" ;;
      "mlock")        MMLOCK="--mlock" ;;
      "no-mmap")      NO_MMAP="--no-mmap" ;;
      "mirostat 1")   MIROSTAT_1="--mirostat" ;;
    esac
  done
}





# create a dialog menu with parameters
#
function parameters_menu() {
  parameters=$(dialog --title "Parameters" --no-cancel --form "Enter the parameter values" 28 80 14 \
  "temperature"         2     3       "0.8"           2     22      7     0 \
  "top_p"               4     3       "0.4"           4     22      7     0 \
  "max_tokens"          6     3       "153"           6     22      7     0 \
  "presence_penalty"    8     3       "0"             8     22      7     0 \
  "frequency_penalty"   2     42      "0"             2     62      7     0 \
  "placeholder_i"       4     42      "23"            4     62      7     0 \
  "placeholder_ii"      6     42      "19"            6     62      7     0 \
  "placeholder_iii"     8     42      "29"            8     62      7     0 \
  3>&1 1>&2 2>&3)
    IFS=$'\n' read -d '' -ra param_values <<<"$parameters"
    TEMP="${param_values[0]}"
    TOP_P="${param_values[1]}"
    MAX_TOKENS="${param_values[2]}"
    PRESENCE_PENALTY="${param_values[3]}"
    FREQUENCY_PENALTY="${param_values[4]}"
}





# create a dialog menu with individual llamacpp flags OR
# insert OpenAI API Key. Only one of these two will be used
#
function advanced_options_menu() {
  advanced_options=$(dialog --title "Advanced Options" --form  "Enter llama.cpp flags manually or OpenAI API Key. It's more secure to use env OPENAI_API_KEY:" \
    28 80 12 \
    "Additional Llama.cpp Flags:"    2   6 ""      2   22   50   0 \
    "OPENAI_API_KEY:"     4   6 ""      4   22   50   0 \
    3>&1 1>&2 2>&3)
      IFS=$'\n' read -d '' -ra adv_values <<<"$advanced_options"
      ADVANCED_FLAGS="${adv_values[0]}"
      OPENAI_API_KEY="${adv_values[1]}"
}





# not ready yet. you can save files, but the program logic is not implemented well until yet
#
function save_config_file() {

    config_filename=$(dialog --stdout --inputbox "Gib den Namen der Konfigurationsdatei ein:" 28 80)
      if [ -n "$config_filename" ]; then
      echo "$checked_options"$'\n'"$parameters"$'\n'"$advanced_options" > "camalls_files/profiles/$config_filename"
      fi
}





# not ready yet - load config files, hopefully also saved sessions etc
#
function load_config_file() {

    config_files=($(ls camalls_files/profiles))
    menu_items=""
        for i in "${!config_files[@]}"; do
            menu_items="${menu_items} $((i+1)) \"${config_files[$i]}\""
        done

    selected_config_index=$(dialog --stdout --no-cancel --menu "Wähle eine gespeicherte Konfigurationsdatei aus:" \
    28 80 "${#config_files[@]}" ${menu_items})

    if [ -n "$selected_config_index" ] && [ "$selected_config_index" -ge 1 ] && [ "$selected_config_index" -le "${#config_files[@]}" ]; then
            config_content=$(cat "camalls_files/profiles/${config_files[$((selected_config_index-1))]}")
            IFS=$'\n' read -d '' -ra config_lines <<<"$config_content"
            checked_options="${config_lines[0]}"
            parameters="${config_lines[1]}"
            advanced_options="${config_lines[2]}"
    fi
}





# this simulates a text stream. i find that this improves the ux 
#
function fake_stream_fast() {
# Überprüfen, ob Eingaben über eine Pipe kommen
if [ -p /dev/stdin ]; then
# stdin in  Variable 'request' lesen
  request=$(cat)
else
# Inhalt der Datei in die Variable 'request' lesen
  request=$(cat "$1")
fi

# Taktgeschwindigkeit
#takt=${2:-1.1}
takt=0.0010

# Zeichen nacheinander ausgeben und für die Taktgeschwindigkeit pausieren
for (( i=0; i<${#request}; i++ )); do
  printf "${request:$i:1}"
  sleep $takt
done

echo
}





# This function allows you to make a pact with the devil...
# oh i mean to interact with the 'open'AI devel_oped API
#
function openai_request_tui() {
tput clear
tput home
#
#
touch chatlog_openai.txt
tempfile="chatlog_openai.txt"
#
# Initialisiere das tempfile
echo "Chatverlauf:" > "$tempfile"
#
# Stelle sicher, dass die Umgebungsvariable gesetzt ist
if [[ -z "$OPENAI_API_KEY" ]]; then
  echo "Your OPENAI_API_KEY is not set as an environment variable. Please set it and try again."
  exit 1
fi
echo "type "exit" to quit"
sleep 1
tput clear
tput home

while true; do
  # read user input
  echo " "
  printf "User: "
  read -r user_input

  # type "exit" to exit
  if [[ "$user_input" == "exit" ]]; then
    break
  fi
  echo "User: $user_input" >> "$tempfile"
  echo -e "\n " >> "$tempfile"
  response=$(curl --silent https://api.openai.com/v1/chat/completions \
    -H 'Content-Type: application/json' \
    -H "Authorization: Bearer $OPENAI_API_KEY" \
    -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.5,
    "stream": false,
    "messages": [{"role": "system", "content": "You are a new, innovative and emotive AI assistant. Always answer in an informative and at the same time narrative style, even if it is about facts. Always try to inspire the user with your answer and arouse their own enthusiasm."}, {"role": "user", "content": "'"${user_input}"'"}]}')

  content=$(echo "$response" | grep -oP '(?<="content":")[^"]*')
  no_spaces=$(echo "$content" | awk '{if (last_char == "" || last_char ~ /[[:punct:]]/ || $0 ~ /^[[:punct:]]/) printf "%s", $0; else printf "%s%s", OFS, $0; last_char = substr($0, length, 1)}')

  echo " "
  echo "ChatGPT: ${no_spaces}" | fake_stream_fast
  echo "ChatGPT: ${no_spaces}" >> "$tempfile"
  echo " " >> "$tempfile"
done
}





# run llama TODO improved version with acces to bert.cpp, gpt-2, bark, whisper, localAI
#
function run_llama_terminal() {
exit
}





# currently (2023-05-21) used code to run llama
#
function beta_llama_terminal() {
  if [[ -z "$MODEL" ]]; then
  echo "Please choose a model first"
  choose_model
    tput clear
  tput home
  eval "./main -m \"$selected_model\" -ins --color --temp \"$TEMP\" --top_p \"$TOP_P\" -c \"$MAX_TOKENS\" $ADVANCED_FLAGS"
else
  tput clear
  tput home
  eval "./main -m \"$selected_model\" -ins --color --temp \"$TEMP\" --top_p \"$TOP_P\" -c \"$MAX_TOKENS\" $ADVANCED_FLAGS"
fi
}





# create a help file and make it lookiing fancy ;)
# if this fails, please try awk instead. see the other function below 
# 
function create_help_file() {

  touch ./camalls_files/help_llamacpp.txt
    ./main --help 2> ./camalls_files/help_llamacpp.txt

    cp ./camalls_files/help_llamacpp.txt ./camalls_files/HELP.txt # this will persist as reference in folder
    cp ./camalls_files/help_llamacpp.txt ./camalls_files/yelp_llamacpp.txt # this will be processed


    # replace poly (three or more) spaces with two spaces
    sed -i 's/   */  /g' ./camalls_files/yelp_llamacpp.txt
    # replace two spaces followed by "-"" with newline and one "-"
    sed -i 's/  -/\n-/g' ./camalls_files/yelp_llamacpp.txt
    # replace newline followed by two spaces with only one newline
    sed -i '/^  /s/  //g' ./camalls_files/yelp_llamacpp.txt
    # replace two spaces with newline
    sed -i 's/  /\n/g' ./camalls_files/yelp_llamacpp.txt


    sleep 0.1
    fold -s -w 70 ./camalls_files/yelp_llamacpp.txt > ./camalls_files/help_llamacpp.txt
    sleep 0.1

  rm ./camalls_files/yelp_llamacpp.txt
}





# alternative methode
#
function awk_help_file() {

  touch ./camalls_files/help_llamacpp.txt
    ./main --help 2> ./camalls_files/help_llamacpp.txt

    cp ./camalls_files/help_llamacpp.txt HELP.txt # this will persist as reference in folder
    cp ./camalls_files/help_llamacpp.txt yelp_llamacpp.txt # this will be processed


    # replace poly (three or more) spaces with two spaces
    awk '{gsub(/   +/, "  "); print}' ./camalls_files/yelp_llamacpp.txt > ./camalls_files/yelp_llamacpp_tmp.txt && mv ./camalls_files/yelp_llamacpp_tmp.txt ./camalls_files/yelp_llamacpp.txt

    # replace two spaces followed by "-"" with newline and one "-"
    awk '{gsub(/  -/, "\n-"); print}' ./camalls_files/yelp_llamacpp.txt > ./camalls_files/yelp_llamacpp_tmp.txt && mv ./camalls_files/yelp_llamacpp_tmp.txt ./camalls_files/yelp_llamacpp.txt

    # replace newline followed by two spaces with only one newline
    awk 'NR > 1 && /^[[:blank:]]{2}/{gsub(/^[[:blank:]]{2}/, "")}{print}' ./camalls_files/yelp_llamacpp.txt > ./camalls_files/yelp_llamacpp_tmp.txt && mv ./camalls_files/yelp_llamacpp_tmp.txt ./camalls_files/yelp_llamacpp.txt

    #replace two spaces with newline
    awk '{gsub(/  /, "\n"); print}' ./camalls_files/yelp_llamacpp.txt > ./camalls_files/yelp_llamacpp_tmp.txt && mv ./camalls_files/yelp_llamacpp_tmp.txt ./camalls_files/yelp_llamacpp.txt

    sleep 0.1
    fold -s -w 70 ./camalls_files/yelp_llamacpp.txt > ./camalls_files/help_llamacpp.txt
    sleep 0.1

  rm ./camalls_files/yelp_llamacpp.txt
}





# implement the main TUI. note: since dialog is not really powerfill that,
# (or i am not aware of more options) a certain consisty of the main has
# to be simulated with a while loop
#
while true; do 


#######  TODO for debugging reasons  ########################
# echo "menu_choice: $menu_choice" >&2                      #
# echo "selected_config_index: $selected_config_index" >&2  #
#############################################################


  menu_choice=$(dialog --title "CamalL" --no-cancel --menu "Choose your preferred options" 28 80 8 \
  1 "Models" \
  2 "Basic Flags" \
  3 "Parameters" \
  4 "Prompt File" \
  5 "Advanced & API KEY" \
  6 "Placeholder (openai)" \
  7 "Save Configs as Profile" \
  8 "Load a Profile" \
  9 "Run LlaMA, Run!" \
  10 "Help" \
  11 "Exit" \
  3>&1 1>&2 2>&3)

  case $menu_choice in
    1)
      choose_model
      ;;
    2)
      basic_flags_menu
      ;;
    3)
      parameters_menu
      ;;
    4)
      # Here we ask the user to enter a prompt and call process_user_input
      FILE_PATH=$(dialog --stdout --fselect $HOME/ 18 80)
      ;;
    5)
      advanced_options_menu
      ;;
    6)
      # PLACEHOLDER
    # if [ "$DECIDE" == "0" ]; then
    #     call_openai_request_tui=1
    #     break
    # else
    #   clear
    #   run_llama_terminal
    #   fi
      openai_request_tui
      ;;
    7)
      # save
      save_config_file
      ;;
    8)
      # load
      load_config_file
      ;;
    9)
      clear
      beta_llama_terminal
      break
      ;;
    10)
      # Help Menu is output of ./main --help
      create_help_file
      dialog --title "Help" --exit-label "Go Back" --textbox "./camalls_files/help_llamacpp.txt" 28 80
      ;;
    11) 
      clear
      break
      ;;
    *)
      ;;
  esac
done